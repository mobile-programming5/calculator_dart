import 'package:dart_application_1/dart_application_1.dart'
    as dart_application_1;
import 'dart:io';

void main() {
  print('MENU');
  print('Select the choice you want to perform : ');
  print('1. ADD\n2. SUBTRACT\n3. MULTIPLY\n4. DIVIDE\n5. EXIT');
  print('Choice you want to enter : ');
  int? menu = int.parse(stdin.readLineSync()!);
  if (menu != 5) {
    print('Enter the value for x : ');
    int? x = int.parse(stdin.readLineSync()!);
    print('Enter the value for y : ');
    int? y = int.parse(stdin.readLineSync()!);
    int sum = 0;
    switch (menu) {
      case 1:
        {
          sum = x + y;
        }
        break;
      case 2:
        {
          sum = x - y;
        }
        break;
      case 3:
        {
          sum = x * y;
        }
        break;
      case 4:
        {
          sum = x ~/ y;
        }
        break;
    }
    print('Sum of the two number is : ');
    print(sum);
  } else if (menu == 5) {
    print('BYE BYE');
  }
}
