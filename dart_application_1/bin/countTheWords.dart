import 'package:dart_application_1/dart_application_1.dart'
    as dart_application_1;
import 'dart:io';

void main() {
  print("Enter your information : ");
  String input = stdin.readLineSync()!;
  var chars = input.toLowerCase().split(" ");
  var counts = <String, int>{};
  for (var char in chars) {
    counts[char] = (counts[char] ?? 0) + 1;
  }
  print("Duplicate words = ${counts}");
  var uniqueStrings = <String>{};
  uniqueStrings.addAll(chars);
  print("Unique Strings = ${uniqueStrings}");
}
